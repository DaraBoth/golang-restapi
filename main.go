package main

import (
	"context"
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/postgresql"
	"io"
	"log"
	"net/http"
	"time"
)

const (
	username = "kjjelxjh"
	password = "lfrM5dzzIODpETfrSmRskIGZ-W8kAeg-"
	hostname = "chunee.db.elephantsql.com:5432"
	dbname   = "kjjelxjh"
)

func dsn(dbName string) string {
	return fmt.Sprintf("%s:%s@tcp(%s)/%s", username, password, hostname, dbName)
}

func main() {
	db, err := sql.Open("posgresql", dsn(""))
	if err != nil {
		log.Printf("Error %s when opening DB\n", err)
		return
	}
	defer db.Close()

	ctx, cancelfunc := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelfunc()
	res, err := db.ExecContext(ctx, "CREATE DATABASE IF NOT EXISTS "+dbname)
	if err != nil {
		log.Printf("Error %s when creating DB\n", err)
		return
	}
	no, err := res.RowsAffected()
	if err != nil {
		log.Printf("Error %s when fetching rows", err)
		return
	}
	log.Printf("rows affected %d\n", no)

	db.Close()
	db, err = sql.Open("mysql", dsn(dbname))
	if err != nil {
		log.Printf("Error %s when opening DB", err)
		return
	}
	defer db.Close()

	db.SetMaxOpenConns(20)
	db.SetMaxIdleConns(20)
	db.SetConnMaxLifetime(time.Minute * 5)

	ctx, cancelfunc = context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelfunc()
	err = db.PingContext(ctx)
	if err != nil {
		log.Printf("Errors %s pinging DB", err)
		return
	}
	log.Printf("Connected to DB %s successfully\n", dbname)

	port := "8000"
	homeHandler := func(w http.ResponseWriter, req *http.Request) {

		io.WriteString(w, "Hello, world!\n")

	}

	http.HandleFunc("/", homeHandler)
	log.Println("Listing for requests at http://localhost:" + port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
